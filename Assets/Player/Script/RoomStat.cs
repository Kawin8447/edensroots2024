using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName = "Data", menuName = "RoomStat")]
public class RoomStat : ScriptableObject
{
    public int money;
    public int floor;
    public int puzzleRoomClear;
    public int enemyRoomClear;
}
