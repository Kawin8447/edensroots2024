using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletProjectileScript : MonoBehaviour
{
    private Vector3 offsetPoint;

    [SerializeField] private float projectileSpeed;

    [SerializeField] private float maxProjectileDistance;
    // Start is called before the first frame update
    void Start()
    {
        offsetPoint = transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        MoveBullet();
    }

    void MoveBullet()
    {
        if (Vector3.Distance(offsetPoint, transform.position) > maxProjectileDistance)
        {
            Destroy(gameObject);
        }
        else
        {
            transform.Translate(Vector3.forward * projectileSpeed * Time.deltaTime);
        }
    }
}
