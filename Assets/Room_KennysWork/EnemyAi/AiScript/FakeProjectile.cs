using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class FakeProjectile : MonoBehaviour
{
    private GameObject _player;
    public GameObject MortaProjectile;
    void Awake()
    {
        _player = GameObject.Find("PlayerManage(Clone)");
    }
    void Start()
    {
        transform.position = new Vector3(transform.position.x, 6, transform.position.z);
    }

    void FixedUpdate()
    {
        transform.Translate(Vector3.up * (Time.deltaTime * 4f));

        if (transform.position.y >= 15)
        {
            IsDestroy();
            Destroy(gameObject);
        }
    }

    private void IsDestroy()
    {
        Instantiate(MortaProjectile, _player.transform.position, Quaternion.identity);
    }
}
