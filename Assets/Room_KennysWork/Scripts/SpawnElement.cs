using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnElement : MonoBehaviour
{
    public GameObject[] itemPrefabs;
    public float spawnRadius = 5f;
    public float spawnChance = 0.5f;
    public float spawnRate = 1f;
    private float nextSpawnTime;

    private void FixedUpdate()
    {
        if (Time.time >= nextSpawnTime)
        {
            if (Random.value < spawnChance)
            {
                Vector3 spawnPosition = transform.position + Random.insideUnitSphere * spawnRadius;

                GameObject itemPrefab = itemPrefabs[Random.Range(0, itemPrefabs.Length)];

                Instantiate(itemPrefab, spawnPosition, Quaternion.identity);
            }

            nextSpawnTime = Time.time + 1f / spawnRate;
        }
    }
}
