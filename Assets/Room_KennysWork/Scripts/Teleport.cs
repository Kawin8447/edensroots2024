using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class Teleport : MonoBehaviour
{
    public static Teleport instance;

    void Awake()
    {
        instance = this;
    }

    public enum ToRoom
    {
        Room1,
        FinishRoom,
    }
    public ToRoom TpToRoom;

    private GameObject target;
    private GameObject player;


    public GameObject Room1Spot;
    public GameObject FinishRoomSpot;

    public void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            GameObject _player = GameObject.Find("PlayerManage(Clone)");
            target = other.gameObject;
            player = target.transform.GetChild(1).gameObject;


            if (_player != null)
            {
                switch (TpToRoom)
                {
                    case ToRoom.Room1:
                        TpRoom(Room1Spot);
                        MinimapManager.Instance.playerIsInField = true;
                        MinimapManager.Instance.playerIsInOtherRoom = false;

                        break;
                    case ToRoom.FinishRoom:
                        TpRoom(FinishRoomSpot);
                        break;
                }
            }
        }
    }

    public void TpRoom(GameObject AnRoom)
    {
        player.GetComponent<CharacterController>().enabled = false;
        player.transform.position = AnRoom.transform.position;
        player.GetComponent<CharacterController>().enabled = true;
    }
}
