using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using Unity.Mathematics;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class GameRoomManager : PureSingleton<GameRoomManager>
{
    private new void Awake()
    {
        ResetDefault();

        HideMarker();

        Transition.SetTrigger("End");
    }

    [Header("Player Spawn Point")]
    public GameObject Player;
    public GameObject PlayerSpawnPoint;

    [Header("-Check clear status of each room")]
    public bool Room1Clear;
    public bool Room2Clear;
    public bool Room3Clear;
    public bool Room4Clear;
    public bool Room5Clear;
    public bool Room6Clear;
    public bool Room7Clear;

    [Header("-Check if each room can clear")]
    public bool FristTimeEnterRoom1;
    public bool FristTimeEnterRoom2;
    public bool FristTimeEnterRoom3;
    public bool FristTimeEnterRoom4;
    public bool FristTimeEnterRoom5;
    public bool FristTimeEnterRoom6;
    public bool FristTimeEnterRoom7;

    [Header("-Gates in each room")]
    public GameObject[] GateRoom;

    [Header("-Range Enemy inside each room")]
    public int RangeEnemyRoom1;
    public int RangeEnemyRoom2;
    public int RangeEnemyRoom3;
    public int RangeEnemyRoom4;
    public int RangeEnemyRoom5;
    public int RangeEnemyRoom6;
    public int RangeEnemyRoom7;

    [Header("-Close Enemy inside each room")]
    public int MeleeEnemyRoom1;
    public int MeleeEnemyRoom2;
    public int MeleeEnemyRoom3;
    public int MeleeEnemyRoom4;
    public int MeleeEnemyRoom5;
    public int MeleeEnemyRoom6;
    public int MeleeEnemyRoom7;

    [Header("-Current Enemy inside current room")]
    public int CurrentEnemyCount;

    [HideInInspector] public int Floor = 1;

    [Header("Transition")]
    public Animator Transition;

    [Header("About Reset game")]
    public GameObject GameoverPanel;
    public TextMeshProUGUI TotalFloortext;
    public TextMeshProUGUI GameoverHeaderText;

    [Header("About Clear game")]
    public GameObject ClearPanel;
    public TextMeshProUGUI TotalFloortext_ClearPanel;

    [Header("Text show Current element")]
    public TextMeshProUGUI _currentElementText;
    [HideInInspector] public string _currentElementVar;

    [Header("PauseMenu")] public GameObject pauseMenu;

    [Header("Timer Text")]
    public TextMeshProUGUI[] timerText;
    private float elapsedTime = 0f;
    private int minutes;
    private int seconds;

    [Header("Marker Of Each Room")]
    public GameObject[] marker;

    private void Update()
    {
        elapsedTime += Time.deltaTime;
        minutes = Mathf.FloorToInt(elapsedTime / 60f);
        seconds = Mathf.FloorToInt(elapsedTime % 60f);
        timerText[0].text = string.Format("Time: {0:00}:{1:00}", minutes, seconds);

        SetElementText();
    }

    public void ResetDefault()
    {
        Time.timeScale = 1;

        Room1Clear = false;
        Room2Clear = false;
        Room3Clear = false;
        Room4Clear = false;
        Room5Clear = false;
        Room6Clear = false;
        Room7Clear = false;

        FristTimeEnterRoom1 = true;
        FristTimeEnterRoom2 = true;
        FristTimeEnterRoom3 = true;
        FristTimeEnterRoom4 = true;
        FristTimeEnterRoom5 = true;
        FristTimeEnterRoom6 = true;
        FristTimeEnterRoom7 = true;

        for (int i = 0; i < GateRoom.Length; i++)
        {
            GateRoom[i].SetActive(false);
        }

        GameRoomManager.Instance.Floor = 1;

        Instantiate(Player, PlayerSpawnPoint.transform.position, Quaternion.identity);
    }

    public void GameOver()
    {
        int Rand = Random.Range(1, 6);
        if (Rand == 1)
        {
            GameoverHeaderText.text = "So Ur HP <= 0";
        }
        if (Rand == 2)
        {
            GameoverHeaderText.text = "HeHeHeHa You lose";
        }
        if (Rand == 3)
        {
            GameoverHeaderText.text = "Try Again or go Home";
        }
        if (Rand == 4)
        {
            GameoverHeaderText.text = "Dead!";
        }
        if (Rand == 1)
        {
            GameoverHeaderText.text = "OwO";
        }

        GameoverPanel.SetActive(true);
        TotalFloortext.text = "Total Floor: " + Floor;
        CallTimeUpdate();
        Time.timeScale = 0;
    }

    public void CampaignClear()
    {
        ClearPanel.SetActive(true);
        TotalFloortext_ClearPanel.text = "Total Floor: " + Floor;
        CallTimeUpdate();
        Time.timeScale = 0;
    }

    public void GameOverRestart()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    public void GameOverGoToMenu()
    {
        Time.timeScale = 1;
        SceneManager.LoadScene(sceneBuildIndex: 0);
    }

    public void GameOverExit()
    {
        Application.Quit();
    }

    public void SetElementText()
    {
        _currentElementText.text = "Element: " + _currentElementVar;
    }

    public void CallPausePanel()
    {
        pauseMenu.SetActive(!pauseMenu.activeSelf);
        CallTimeUpdate();
    }

    public void CallTimeUpdate()
    {
        for (int i = 0; i < timerText.Length; i++)
        {
            timerText[i].text = string.Format("Time: {0:00}:{1:00}", minutes, seconds);
        }

        /*timerText[1].text = string.Format("Time: {0:00}:{1:00}", minutes, seconds);
        timerText[2].text = string.Format("Time: {0:00}:{1:00}", minutes, seconds);
        timerText[3].text = string.Format("Time: {0:00}:{1:00}", minutes, seconds);*/
    }

    public void HideMarker()
    {
        for (int i = 0; i < marker.Length; i++)
        {
            marker[i].SetActive(false);
        }
    }
}
