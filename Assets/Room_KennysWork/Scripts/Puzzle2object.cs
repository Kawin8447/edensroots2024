using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Puzzle2object : MonoBehaviour,Damageable
{
    private float _Slider = 0.501f;
    private ParticleSystem _particleSystem;
    private _PuzzelTest setNum;
    private string PlayerElement;
    private Material _material;
    public string BlockElement;
    public float Hitnumber;
    private bool isblockClear;
    void Awake()
    {
        setNum = GetComponentInParent<_PuzzelTest>();
        _particleSystem = GetComponentInChildren<ParticleSystem>();
        _material = GetComponent<MeshRenderer>().sharedMaterial;
        _material.SetFloat("_Fillwide",0.501f);
        isblockClear = false;
    }

    public void DoDamage(float damage)
    {
        PlayerElement = GameRoomManager.Instance._currentElementVar;
        if (!isblockClear)
        {
            if (BlockElement == PlayerElement)
            {
                _Slider -= Hitnumber;
            }
            if (_Slider < -0.5f)
            {
                StartCoroutine(inten());
            }
            _material.SetFloat("_Fillwide",_Slider);
        }
    }
    IEnumerator inten()
    {
        _particleSystem.Play();
        isblockClear = true;
        yield return new WaitForSeconds(0.2f);
        setNum.blockLeft--;
    }
}
