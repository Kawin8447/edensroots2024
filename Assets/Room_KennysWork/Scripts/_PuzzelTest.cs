using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class _PuzzelTest : MonoBehaviour
{
    public static event System.Action<bool> PuzzelRoomClear;

    public int blockLeft = 6;

    private void Update()
    {
        if (blockLeft <= 0)
        {
            PuzzelRoomClear?.Invoke(true);
            Destroy(gameObject, 0.5f);
        }
    }
}