using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Puzzle3object : MonoBehaviour,Damageable
{
    public int digit;
    private MeshRenderer _material;
    private string BlockElement;
    private string PlayerElement;
    public int _pInput;
    private bool canHit;
    void Awake()
    {
        digit = Random.Range(1, 4);
        _pInput = 0;
        _material = GetComponent<MeshRenderer>();
        StartCoroutine(RandomNum());
    }
    public void DoDamage(float damage)
    {
        PlayerElement = GameRoomManager.Instance._currentElementVar;
        if (_pInput == 0 && canHit == true)
        {
            if (PlayerElement== "Water")
            {
                _pInput = 1;
                _material.material.SetColor("_BaseColor",Color.HSVToRGB(0.53f,0.62f,0.98f));
            }else if (PlayerElement== "Earth")
            {
                _pInput = 2;
                _material.material.SetColor("_BaseColor",Color.HSVToRGB(0.07f,1.00f,0.90f));
            }
            else if (PlayerElement== "Wind")
            {
                _pInput = 3;
                _material.material.SetColor("_BaseColor",Color.HSVToRGB(0.33f,0.66f,0.93f));
            }
        }
    }
    IEnumerator RandomNum()
    {
        canHit = false;
        switch (digit)
        {
            case 1: _material.material.SetColor("_BaseColor",Color.HSVToRGB(0.53f,0.62f,0.98f));
                break;
            case 2: _material.material.SetColor("_BaseColor",Color.HSVToRGB(0.07f,1.00f,0.90f));
                break;
            case 3: _material.material.SetColor("_BaseColor",Color.HSVToRGB(0.33f,0.66f,0.93f));
                break;
        }
        yield return new WaitForSeconds(5f);
        canHit = true;
        _material.material.SetColor("_BaseColor",Color.gray);
    }
    public void resetColor()
    {
        _pInput = 0;
        StartCoroutine(RandomNum());
    }
}
