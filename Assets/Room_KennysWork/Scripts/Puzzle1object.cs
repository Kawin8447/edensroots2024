using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Puzzle1object : MonoBehaviour,Damageable
{
    private float HealthTest = 60;
    private _PuzzelTest setNum;
    private string PlayerElement;
    public string BlockElement;
    private ParticleSystem _particleSystem;
    private void Awake()
    {
        setNum = GetComponentInParent<_PuzzelTest>();
        _particleSystem = GetComponent<ParticleSystem>();
    }

    public void DoDamage(float damage)
    {
        PlayerElement = GameRoomManager.Instance._currentElementVar;
        if (BlockElement == PlayerElement)
        {
            HealthTest -= damage;
            if (HealthTest > 0)
            {
                StartCoroutine(inten());
            }
        }
        if (HealthTest <= 0)
        {
            setNum.blockLeft--;
            Destroy(gameObject);
        }
    }
    IEnumerator inten()
    {
        _particleSystem.Play();
        yield return new WaitForSeconds(0.2f);
    }
}
