using System;
using System.Collections;
using System.Collections.Generic;
using Cinemachine;
using Photon.Realtime;
using TMPro;
using Unity.Mathematics;
using Unity.VisualScripting;
using UnityEditor.Rendering;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class InsideRoomManager : PureSingleton<InsideRoomManager>
{
    [Header("Enemy prefab")]
    public GameObject RangeEnemyPrefab;
    public GameObject MeleeEnemyPrefab;

    [Header("Ai spawn point")]
    public GameObject[] EachAiSpawnpoint;

    [Header("Each Puzzel type")]
    public GameObject[] puzzelType;

    [Header("Center of each room")]
    public GameObject[] CenterOfRoom;

    [Header("Objective Displayer")]
    public TextMeshProUGUI _currentEnemyText;
    public GameObject EnemyStatusCard;
    public GameObject PuzzelStatusCard;

    private List<int> previousRandomNumbers = new List<int>();
    private const int MAX_DUPLICATES = 2;
    private const int MIN_RANDOM_NUMBER = 1;
    private const int MAX_RANDOM_NUMBER = 2;

    public int[] typeRoom;

    private Image[] FightRoomIcon;
    private Image[] PuzzelRoomIcon;

    [Header("Boss Campaign")]
    public GameObject BossSpawnPoint;
    public GameObject BossPrefab;
    private bool firstTimeBossRoom = true;

    [Header("RoomStats")]
    public RoomStat _roomStats;
    public GameObject RoomClearText;
    public GameObject CampaignClearText;

    private void OnEnable()
    {
        RoomTriggerCheck.PlayerIsOnTheRoom += PlayerEnterRoom;

        EnemyRangeAi.RangeRoomClear += RoomIsClear;
        EnemyMeleeAi.MeleeRoomClear += RoomIsClear;
        _PuzzelTest.PuzzelRoomClear += RoomIsClear;

        EnemyBossAi.BossRoomClear += CampaignClear;
    }

    private void OnDisable()
    {
        RoomTriggerCheck.PlayerIsOnTheRoom -= PlayerEnterRoom;

        EnemyRangeAi.RangeRoomClear -= RoomIsClear;
        EnemyMeleeAi.MeleeRoomClear -= RoomIsClear;
        _PuzzelTest.PuzzelRoomClear -= RoomIsClear;

        EnemyBossAi.BossRoomClear -= CampaignClear;
    }

    private new void Awake()
    {
        RandomNewRoomType();

        typeRoom[0] = 1;
    }

    private void Start()
    {
        EnemyStatusCard.SetActive(false);
        PuzzelStatusCard.SetActive(false);

        UpdateIcon();

        RoomClearText.gameObject.SetActive(false);
        CampaignClearText.gameObject.SetActive(false);
    }

    public void RandomNewRoomType()
    {
        Debug.LogWarning("New Room Type Just Random");
        typeRoom[0] = Random.Range(1, 4);
        typeRoom[1] = Random.Range(1, 4);
        typeRoom[2] = Random.Range(1, 4);
        typeRoom[3] = Random.Range(1, 4);
        typeRoom[4] = Random.Range(1, 4);
        typeRoom[5] = Random.Range(1, 4);
        typeRoom[6] = Random.Range(1, 4);
    }

    public void UpdateIcon()
    {
        Debug.LogWarning("Update icon");
    }

    private void Update()
    {
        _currentEnemyText.text = "Enemy Remaining: " + GameRoomManager.Instance.CurrentEnemyCount;
    }

    private void PlayerEnterRoom(string roomName)
    {
        if (roomName == "PlayerIsOnRoom1")
        {
            Debug.Log("Player has enter room1");
            if (GameRoomManager.Instance.Room1Clear == false && GameRoomManager.Instance.FristTimeEnterRoom1 == true)
            {
                GameRoomManager.Instance.FristTimeEnterRoom1 = false;

                //ปิดประตู
                GameRoomManager.Instance.GateRoom[0].SetActive(true);

                //ห้องสู้
                if (typeRoom[0] == 1 || typeRoom[0] == 3)
                {
                    Debug.LogWarning("Enemy spawned");
                    EnemyStatusCard.SetActive(true);
                    GameRoomManager.Instance.CurrentEnemyCount = GameRoomManager.Instance.RangeEnemyRoom1 + GameRoomManager.Instance.MeleeEnemyRoom1;

                    //สปอนตีใกล
                    for (int i = 0; i < GameRoomManager.Instance.RangeEnemyRoom1; i++)
                    {
                        float spawnRadius = 10f;
                        float randomAngle = Random.Range(0f, 2f * Mathf.PI);

                        float newSpawnPosX = EachAiSpawnpoint[0].transform.position.x + spawnRadius * Mathf.Cos(randomAngle);
                        float newSpawnPosZ = EachAiSpawnpoint[0].transform.position.z + spawnRadius * Mathf.Sin(randomAngle);

                        Vector3 SpawnPos = new Vector3(newSpawnPosX, EachAiSpawnpoint[0].transform.position.y, newSpawnPosZ);

                        Instantiate(RangeEnemyPrefab, SpawnPos, quaternion.identity);
                    }
                    //สปอนตีใกล้
                    for (int i = 0; i < GameRoomManager.Instance.MeleeEnemyRoom1; i++)
                    {
                        float spawnRadius = 15f;
                        float randomAngle = Random.Range(0f, 2f * Mathf.PI);

                        float newSpawnPosX = EachAiSpawnpoint[0].transform.position.x + spawnRadius * Mathf.Cos(randomAngle);
                        float newSpawnPosZ = EachAiSpawnpoint[0].transform.position.z + spawnRadius * Mathf.Sin(randomAngle);

                        Vector3 SpawnPos = new Vector3(newSpawnPosX, EachAiSpawnpoint[0].transform.position.y, newSpawnPosZ);

                        Instantiate(MeleeEnemyPrefab, SpawnPos, quaternion.identity);
                    }
                }
                //ห้องปริศนา
                if (typeRoom[0] == 2)
                {
                    Debug.LogWarning("Puzzel spawned");
                    PuzzelStatusCard.SetActive(true);
                    int randPuzzelType = Random.Range(0, 3);
                    Instantiate(puzzelType[randPuzzelType], CenterOfRoom[0].transform.position, quaternion.identity);
                }
            }
        }

        if (roomName == "PlayerIsOnRoom2")
        {
            Debug.Log("Player has enter room2");

            if (GameRoomManager.Instance.Room2Clear == false && GameRoomManager.Instance.FristTimeEnterRoom2 == true)
            {
                GameRoomManager.Instance.FristTimeEnterRoom2 = false;

                //ปิดประตู
                GameRoomManager.Instance.GateRoom[1].SetActive(true);

                //ห้องสู้
                if (typeRoom[1] == 1 || typeRoom[1] == 3)
                {
                    Debug.LogWarning("Enemy spawned");
                    EnemyStatusCard.SetActive(true);
                    GameRoomManager.Instance.CurrentEnemyCount = GameRoomManager.Instance.RangeEnemyRoom2 + GameRoomManager.Instance.MeleeEnemyRoom2;

                    //สปอนตีใกล
                    for (int i = 0; i < GameRoomManager.Instance.RangeEnemyRoom2; i++)
                    {
                        float spawnRadius = 10f;
                        float randomAngle = Random.Range(0f, 2f * Mathf.PI);

                        float newSpawnPosX = EachAiSpawnpoint[1].transform.position.x + spawnRadius * Mathf.Cos(randomAngle);
                        float newSpawnPosZ = EachAiSpawnpoint[1].transform.position.z + spawnRadius * Mathf.Sin(randomAngle);

                        Vector3 SpawnPos = new Vector3(newSpawnPosX, EachAiSpawnpoint[1].transform.position.y, newSpawnPosZ);

                        Instantiate(RangeEnemyPrefab, SpawnPos, quaternion.identity);
                    }
                    //สปอนตีใกล้
                    for (int i = 0; i < GameRoomManager.Instance.MeleeEnemyRoom2; i++)
                    {
                        float spawnRadius = 15f;
                        float randomAngle = Random.Range(0f, 2f * Mathf.PI);

                        float newSpawnPosX = EachAiSpawnpoint[1].transform.position.x + spawnRadius * Mathf.Cos(randomAngle);
                        float newSpawnPosZ = EachAiSpawnpoint[1].transform.position.z + spawnRadius * Mathf.Sin(randomAngle);

                        Vector3 SpawnPos = new Vector3(newSpawnPosX, EachAiSpawnpoint[1].transform.position.y, newSpawnPosZ);

                        Instantiate(MeleeEnemyPrefab, SpawnPos, quaternion.identity);
                    }
                }
                //ห้องปริศนา
                if (typeRoom[1] == 2)
                {
                    Debug.LogWarning("Puzzel spawned");
                    PuzzelStatusCard.SetActive(true);
                    int randPuzzelType = Random.Range(0, 3);
                    Instantiate(puzzelType[randPuzzelType], CenterOfRoom[1].transform.position, quaternion.identity);
                }
            }
        }

        if (roomName == "PlayerIsOnRoom3")
        {
            Debug.Log("Player has enter room3");

            if (GameRoomManager.Instance.Room3Clear == false && GameRoomManager.Instance.FristTimeEnterRoom3 == true)
            {
                GameRoomManager.Instance.FristTimeEnterRoom3 = false;

                //ปิดประตู
                GameRoomManager.Instance.GateRoom[2].SetActive(true);

                //ห้องสู้
                if (typeRoom[2] == 1 || typeRoom[2] == 3)
                {
                    Debug.LogWarning("Enemy spawned");
                    EnemyStatusCard.SetActive(true);
                    GameRoomManager.Instance.CurrentEnemyCount = GameRoomManager.Instance.RangeEnemyRoom3 + GameRoomManager.Instance.MeleeEnemyRoom3;

                    //สปอนตีใกล
                    for (int i = 0; i < GameRoomManager.Instance.RangeEnemyRoom3; i++)
                    {
                        float spawnRadius = 10f;
                        float randomAngle = Random.Range(0f, 2f * Mathf.PI);

                        float newSpawnPosX = EachAiSpawnpoint[2].transform.position.x + spawnRadius * Mathf.Cos(randomAngle);
                        float newSpawnPosZ = EachAiSpawnpoint[2].transform.position.z + spawnRadius * Mathf.Sin(randomAngle);

                        Vector3 SpawnPos = new Vector3(newSpawnPosX, EachAiSpawnpoint[2].transform.position.y, newSpawnPosZ);

                        Instantiate(RangeEnemyPrefab, SpawnPos, quaternion.identity);
                    }
                    //สปอนตีใกล้
                    for (int i = 0; i < GameRoomManager.Instance.MeleeEnemyRoom3; i++)
                    {
                        float spawnRadius = 15f;
                        float randomAngle = Random.Range(0f, 2f * Mathf.PI);

                        float newSpawnPosX = EachAiSpawnpoint[2].transform.position.x + spawnRadius * Mathf.Cos(randomAngle);
                        float newSpawnPosZ = EachAiSpawnpoint[2].transform.position.z + spawnRadius * Mathf.Sin(randomAngle);

                        Vector3 SpawnPos = new Vector3(newSpawnPosX, EachAiSpawnpoint[2].transform.position.y, newSpawnPosZ);

                        Instantiate(MeleeEnemyPrefab, SpawnPos, quaternion.identity);
                    }
                }
                //ห้องปริศนา
                if (typeRoom[2] == 2)
                {
                    Debug.LogWarning("Puzzel spawned");
                    PuzzelStatusCard.SetActive(true);
                    int randPuzzelType = Random.Range(0, 3);
                    Instantiate(puzzelType[randPuzzelType], CenterOfRoom[2].transform.position, quaternion.identity);
                }
            }
        }

        if (roomName == "PlayerIsOnRoom4")
        {
            Debug.Log("Player has enter room4");

            if (GameRoomManager.Instance.Room4Clear == false && GameRoomManager.Instance.FristTimeEnterRoom4 == true)
            {
                GameRoomManager.Instance.FristTimeEnterRoom4 = false;

                //ปิดประตู
                GameRoomManager.Instance.GateRoom[3].SetActive(true);

                //ห้องสู้
                if (typeRoom[3] == 1 || typeRoom[3] == 3)
                {
                    Debug.LogWarning("Enemy spawned");
                    EnemyStatusCard.SetActive(true);
                    GameRoomManager.Instance.CurrentEnemyCount = GameRoomManager.Instance.RangeEnemyRoom4 + GameRoomManager.Instance.MeleeEnemyRoom4;

                    //สปอนตีใกล
                    for (int i = 0; i < GameRoomManager.Instance.RangeEnemyRoom4; i++)
                    {
                        float spawnRadius = 10f;
                        float randomAngle = Random.Range(0f, 2f * Mathf.PI);

                        float newSpawnPosX = EachAiSpawnpoint[3].transform.position.x + spawnRadius * Mathf.Cos(randomAngle);
                        float newSpawnPosZ = EachAiSpawnpoint[3].transform.position.z + spawnRadius * Mathf.Sin(randomAngle);

                        Vector3 SpawnPos = new Vector3(newSpawnPosX, EachAiSpawnpoint[3].transform.position.y, newSpawnPosZ);

                        Instantiate(RangeEnemyPrefab, SpawnPos, quaternion.identity);
                    }
                    //สปอนตีใกล้
                    for (int i = 0; i < GameRoomManager.Instance.MeleeEnemyRoom4; i++)
                    {
                        float spawnRadius = 15f;
                        float randomAngle = Random.Range(0f, 2f * Mathf.PI);

                        float newSpawnPosX = EachAiSpawnpoint[3].transform.position.x + spawnRadius * Mathf.Cos(randomAngle);
                        float newSpawnPosZ = EachAiSpawnpoint[3].transform.position.z + spawnRadius * Mathf.Sin(randomAngle);

                        Vector3 SpawnPos = new Vector3(newSpawnPosX, EachAiSpawnpoint[3].transform.position.y, newSpawnPosZ);

                        Instantiate(MeleeEnemyPrefab, SpawnPos, quaternion.identity);
                    }
                }
                //ห้องปริศนา
                if (typeRoom[3] == 2)
                {
                    Debug.LogWarning("Puzzel spawned");
                    PuzzelStatusCard.SetActive(true);
                    int randPuzzelType = Random.Range(0, 3);
                    Instantiate(puzzelType[randPuzzelType], CenterOfRoom[3].transform.position, quaternion.identity);
                }
            }
        }

        if (roomName == "PlayerIsOnRoom5")
        {
            Debug.Log("Player has enter room5");

            if (GameRoomManager.Instance.Room5Clear == false && GameRoomManager.Instance.FristTimeEnterRoom5 == true)
            {
                GameRoomManager.Instance.FristTimeEnterRoom5 = false;

                //ปิดประตู
                GameRoomManager.Instance.GateRoom[4].SetActive(true);

                //ห้องสู้
                if (typeRoom[4] == 1 || typeRoom[4] == 3)
                {
                    Debug.LogWarning("Enemy spawned");
                    EnemyStatusCard.SetActive(true);
                    GameRoomManager.Instance.CurrentEnemyCount = GameRoomManager.Instance.RangeEnemyRoom5 + GameRoomManager.Instance.MeleeEnemyRoom5;

                    //สปอนตีใกล
                    for (int i = 0; i < GameRoomManager.Instance.RangeEnemyRoom5; i++)
                    {
                        float spawnRadius = 10f;
                        float randomAngle = Random.Range(0f, 2f * Mathf.PI);

                        float newSpawnPosX = EachAiSpawnpoint[4].transform.position.x + spawnRadius * Mathf.Cos(randomAngle);
                        float newSpawnPosZ = EachAiSpawnpoint[4].transform.position.z + spawnRadius * Mathf.Sin(randomAngle);

                        Vector3 SpawnPos = new Vector3(newSpawnPosX, EachAiSpawnpoint[4].transform.position.y, newSpawnPosZ);

                        Instantiate(RangeEnemyPrefab, SpawnPos, quaternion.identity);
                    }
                    //สปอนตีใกล้
                    for (int i = 0; i < GameRoomManager.Instance.MeleeEnemyRoom5; i++)
                    {
                        float spawnRadius = 15f;
                        float randomAngle = Random.Range(0f, 2f * Mathf.PI);

                        float newSpawnPosX = EachAiSpawnpoint[4].transform.position.x + spawnRadius * Mathf.Cos(randomAngle);
                        float newSpawnPosZ = EachAiSpawnpoint[4].transform.position.z + spawnRadius * Mathf.Sin(randomAngle);

                        Vector3 SpawnPos = new Vector3(newSpawnPosX, EachAiSpawnpoint[4].transform.position.y, newSpawnPosZ);

                        Instantiate(MeleeEnemyPrefab, SpawnPos, quaternion.identity);
                    }
                }
                //ห้องปริศนา
                if (typeRoom[4] == 2)
                {
                    Debug.LogWarning("Puzzel spawned");
                    PuzzelStatusCard.SetActive(true);
                    int randPuzzelType = Random.Range(0, 3);
                    Instantiate(puzzelType[randPuzzelType], CenterOfRoom[4].transform.position, quaternion.identity);
                }
            }
        }

        if (roomName == "PlayerIsOnRoom6")
        {
            Debug.Log("Player has enter room6");

            if (GameRoomManager.Instance.Room6Clear == false && GameRoomManager.Instance.FristTimeEnterRoom6 == true)
            {
                GameRoomManager.Instance.FristTimeEnterRoom6 = false;

                //ปิดประตู
                GameRoomManager.Instance.GateRoom[5].SetActive(true);

                //ห้องสู้
                if (typeRoom[5] == 1 || typeRoom[5] == 3)
                {
                    Debug.LogWarning("Enemy spawned");
                    EnemyStatusCard.SetActive(true);
                    GameRoomManager.Instance.CurrentEnemyCount = GameRoomManager.Instance.RangeEnemyRoom6 + GameRoomManager.Instance.MeleeEnemyRoom6;

                    //สปอนตีใกล
                    for (int i = 0; i < GameRoomManager.Instance.RangeEnemyRoom6; i++)
                    {
                        float spawnRadius = 10f;
                        float randomAngle = Random.Range(0f, 2f * Mathf.PI);

                        float newSpawnPosX = EachAiSpawnpoint[5].transform.position.x + spawnRadius * Mathf.Cos(randomAngle);
                        float newSpawnPosZ = EachAiSpawnpoint[5].transform.position.z + spawnRadius * Mathf.Sin(randomAngle);

                        Vector3 SpawnPos = new Vector3(newSpawnPosX, EachAiSpawnpoint[5].transform.position.y, newSpawnPosZ);

                        Instantiate(RangeEnemyPrefab, SpawnPos, quaternion.identity);
                    }
                    //สปอนตีใกล้
                    for (int i = 0; i < GameRoomManager.Instance.MeleeEnemyRoom6; i++)
                    {
                        float spawnRadius = 15f;
                        float randomAngle = Random.Range(0f, 2f * Mathf.PI);

                        float newSpawnPosX = EachAiSpawnpoint[5].transform.position.x + spawnRadius * Mathf.Cos(randomAngle);
                        float newSpawnPosZ = EachAiSpawnpoint[5].transform.position.z + spawnRadius * Mathf.Sin(randomAngle);

                        Vector3 SpawnPos = new Vector3(newSpawnPosX, EachAiSpawnpoint[5].transform.position.y, newSpawnPosZ);

                        Instantiate(MeleeEnemyPrefab, SpawnPos, quaternion.identity);
                    }
                }
                //ห้องปริศนา
                if (typeRoom[5] == 2)
                {
                    Debug.LogWarning("Puzzel spawned");
                    PuzzelStatusCard.SetActive(true);
                    int randPuzzelType = Random.Range(0, 3);
                    Instantiate(puzzelType[randPuzzelType], CenterOfRoom[5].transform.position, quaternion.identity);
                }
            }
        }

        if (roomName == "PlayerIsOnRoom7")
        {
            Debug.Log("Player has enter room7");

            if (GameRoomManager.Instance.Room7Clear == false && GameRoomManager.Instance.FristTimeEnterRoom7 == true)
            {
                GameRoomManager.Instance.FristTimeEnterRoom7 = false;

                //ปิดประตู
                GameRoomManager.Instance.GateRoom[6].SetActive(true);

                //ห้องสู้
                if (typeRoom[6] == 1 || typeRoom[6] == 3)
                {
                    Debug.LogWarning("Enemy spawned");
                    EnemyStatusCard.SetActive(true);
                    GameRoomManager.Instance.CurrentEnemyCount = GameRoomManager.Instance.RangeEnemyRoom7 + GameRoomManager.Instance.MeleeEnemyRoom7;

                    //สปอนตีใกล
                    for (int i = 0; i < GameRoomManager.Instance.RangeEnemyRoom7; i++)
                    {
                        float spawnRadius = 10f;
                        float randomAngle = Random.Range(0f, 2f * Mathf.PI);

                        float newSpawnPosX = EachAiSpawnpoint[6].transform.position.x + spawnRadius * Mathf.Cos(randomAngle);
                        float newSpawnPosZ = EachAiSpawnpoint[6].transform.position.z + spawnRadius * Mathf.Sin(randomAngle);

                        Vector3 SpawnPos = new Vector3(newSpawnPosX, EachAiSpawnpoint[6].transform.position.y, newSpawnPosZ);

                        Instantiate(RangeEnemyPrefab, SpawnPos, quaternion.identity);
                    }
                    //สปอนตีใกล้
                    for (int i = 0; i < GameRoomManager.Instance.MeleeEnemyRoom7; i++)
                    {
                        float spawnRadius = 15f;
                        float randomAngle = Random.Range(0f, 2f * Mathf.PI);

                        float newSpawnPosX = EachAiSpawnpoint[6].transform.position.x + spawnRadius * Mathf.Cos(randomAngle);
                        float newSpawnPosZ = EachAiSpawnpoint[6].transform.position.z + spawnRadius * Mathf.Sin(randomAngle);

                        Vector3 SpawnPos = new Vector3(newSpawnPosX, EachAiSpawnpoint[6].transform.position.y, newSpawnPosZ);

                        Instantiate(MeleeEnemyPrefab, SpawnPos, quaternion.identity);
                    }
                }
                //ห้องปริศนา
                if (typeRoom[6] == 2)
                {
                    Debug.LogWarning("Puzzel spawned");
                    PuzzelStatusCard.SetActive(true);
                    int randPuzzelType = Random.Range(0, 3);
                    Instantiate(puzzelType[randPuzzelType], CenterOfRoom[6].transform.position, quaternion.identity);
                }
            }
        }

        if (roomName == "PlayerIsOnBossRoom")
        {
            if (firstTimeBossRoom == true)
            {
                firstTimeBossRoom = false;
                Invoke(nameof(SpawnBoss), 2f);
            }
            Debug.LogWarning("Player has Enter Boss Room");
        }
    }
    private void SpawnBoss()
    {
        Instantiate(BossPrefab, BossSpawnPoint.transform.position, quaternion.identity);
    }

    private void RoomIsClear(bool RoomClear)
    {
        if (RoomClear == true && GameRoomManager.Instance.Room1Clear == false && GameRoomManager.Instance.FristTimeEnterRoom1 == false)
        {
            GameRoomManager.Instance.Room1Clear = true;
            GameRoomManager.Instance.GateRoom[0].SetActive(false);

            ClearObjectivePanel();
            if (typeRoom[0] == 1 || typeRoom[0] == 3)
            {
                HealPlayer(10);
            }

            GameRoomManager.Instance.marker[0].SetActive(true);
            StartCoroutine(ShowRoomClearText());

            //Reward
            _roomStats.money += 5;
        }

        if (RoomClear == true && GameRoomManager.Instance.Room2Clear == false && GameRoomManager.Instance.FristTimeEnterRoom2 == false)
        {
            GameRoomManager.Instance.Room2Clear = true;
            GameRoomManager.Instance.GateRoom[1].SetActive(false);

            ClearObjectivePanel();
            if (typeRoom[1] == 1 || typeRoom[1] == 3)
            {
                HealPlayer(10);
            }

            GameRoomManager.Instance.marker[1].SetActive(true);
            StartCoroutine(ShowRoomClearText());
        }

        if (RoomClear == true && GameRoomManager.Instance.Room3Clear == false && GameRoomManager.Instance.FristTimeEnterRoom3 == false)
        {
            GameRoomManager.Instance.Room3Clear = true;
            GameRoomManager.Instance.GateRoom[2].SetActive(false);

            ClearObjectivePanel();
            if (typeRoom[2] == 1 || typeRoom[2] == 3)
            {
                HealPlayer(10);
            }

            GameRoomManager.Instance.marker[2].SetActive(true);
            StartCoroutine(ShowRoomClearText());
        }

        if (RoomClear == true && GameRoomManager.Instance.Room4Clear == false && GameRoomManager.Instance.FristTimeEnterRoom4 == false)
        {
            GameRoomManager.Instance.Room4Clear = true;
            GameRoomManager.Instance.GateRoom[3].SetActive(false);

            ClearObjectivePanel();
            if (typeRoom[3] == 1 || typeRoom[3] == 3)
            {
                HealPlayer(10);
            }

            GameRoomManager.Instance.marker[3].SetActive(true);
            StartCoroutine(ShowRoomClearText());
        }

        if (RoomClear == true && GameRoomManager.Instance.Room5Clear == false && GameRoomManager.Instance.FristTimeEnterRoom5 == false)
        {
            GameRoomManager.Instance.Room5Clear = true;
            GameRoomManager.Instance.GateRoom[4].SetActive(false);

            ClearObjectivePanel();
            if (typeRoom[4] == 1 || typeRoom[4] == 3)
            {
                HealPlayer(10);
            }

            GameRoomManager.Instance.marker[4].SetActive(true);
            StartCoroutine(ShowRoomClearText());
        }

        if (RoomClear == true && GameRoomManager.Instance.Room6Clear == false && GameRoomManager.Instance.FristTimeEnterRoom6 == false)
        {
            GameRoomManager.Instance.Room6Clear = true;
            GameRoomManager.Instance.GateRoom[5].SetActive(false);

            ClearObjectivePanel();
            if (typeRoom[5] == 1 || typeRoom[5] == 3)
            {
                HealPlayer(10);
            }

            GameRoomManager.Instance.marker[5].SetActive(true);
            StartCoroutine(ShowRoomClearText());
        }

        if (RoomClear == true && GameRoomManager.Instance.Room7Clear == false && GameRoomManager.Instance.FristTimeEnterRoom7 == false)
        {
            GameRoomManager.Instance.Room7Clear = true;
            GameRoomManager.Instance.GateRoom[6].SetActive(false);

            ClearObjectivePanel();
            if (typeRoom[6] == 1 || typeRoom[6] == 3)
            {
                HealPlayer(10);
            }

            GameRoomManager.Instance.marker[6].SetActive(true);
            StartCoroutine(ShowRoomClearText());
        }
    }

    private void ClearObjectivePanel()
    {
        if (EnemyStatusCard)
        {
            EnemyStatusCard.SetActive(false);
        }
        if (PuzzelStatusCard)
        {
            PuzzelStatusCard.SetActive(false);
        }
    }

    public void HealPlayer(int Amount)
    {
        GameObject _player = GameObject.Find("PlayerManage(Clone)");

        if (_player != null)
        {
            PlayerManager _playerManager = _player.GetComponent<PlayerManager>();
            if (_playerManager != null)
            {
                _playerManager.HealPlayer(10);
            }
        }
    }

    public void CampaignClear(bool clear)
    {
        if (clear == true)
        {
            GameObject[] AliveAi = GameObject.FindGameObjectsWithTag("Enemy");
            foreach (GameObject gameObjectWithTag in AliveAi)
            {
                Destroy(gameObjectWithTag);
            }


            HealPlayer(100);

            StartCoroutine(ShowCampaignClearText());
            Invoke(nameof(CallClearCampaign), 3.5f);
        }

    }
    private void CallClearCampaign()
    {
        Debug.LogWarning("Campaign has been clear");
        GameRoomManager.Instance.CampaignClear();
    }
    public IEnumerator ShowRoomClearText()
    {
        yield return new WaitForSeconds(0.25f);

        RoomClearText.gameObject.SetActive(true);

        yield return new WaitForSeconds(3f);

        RoomClearText.gameObject.SetActive(false);
    }

    public IEnumerator ShowCampaignClearText()
    {
        yield return new WaitForSeconds(0.25f);

        CampaignClearText.gameObject.SetActive(true);

        yield return new WaitForSeconds(3f);

        CampaignClearText.gameObject.SetActive(false);
    }
}
