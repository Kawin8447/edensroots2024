using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using UnityEngine;

public class PlayerAction : MonoBehaviourPun
{
    public float rotationSpeed = 5f;

    void Update()
    {
        if (photonView.IsMine)
        {
            // Get the mouse position
            Vector3 mousePosition = Input.mousePosition;

            // Convert the mouse position from screen space to world space
            Ray ray = Camera.main.ScreenPointToRay(mousePosition);
            RaycastHit hit;

            if (Physics.Raycast(ray, out hit))
            {
                // Get the direction from the player position to the mouse position
                Vector3 targetPosition = new Vector3(hit.point.x, transform.position.y, hit.point.z);
                Vector3 lookDirection = targetPosition - transform.position;

                // Ignore rotation around the y-axis to prevent camera rotation
                lookDirection.y = 0f;

                // Rotate the player towards the mouse position smoothly
                Quaternion rotation = Quaternion.LookRotation(lookDirection);
                transform.rotation = Quaternion.Slerp(transform.rotation, rotation, rotationSpeed * Time.deltaTime);
            }
        }
    }
}
