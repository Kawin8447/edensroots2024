using System;
using System.Collections;
using System.Collections.Generic;
using Cinemachine;
using Unity.Mathematics;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.UIElements;
using Photon.Pun;
using UnityEngine.UI;
using Slider = UnityEngine.UI.Slider;

public class PlayerController : MonoBehaviourPunCallbacks
{
    public float rotationSpeed = 5f;

    Rigidbody rb;
    
    PhotonView PV;
    
    private Vector2 _mousepos;
    [SerializeField]private Camera camera1;
    
    private void Awake()
    {
        rb = GetComponent<Rigidbody>();
        PV = GetComponent<PhotonView>();
    }

    private void Start()
    {
        if (!PV.IsMine)
        {
            Destroy(GetComponentInChildren<Camera>().gameObject);
            Destroy(GetComponentInChildren<CinemachineFreeLook>().gameObject);
        }
    }

    void Update()
    {
        
        if (!PV.IsMine)
            return;
        
        Moving();
        //Looking();
        RotateWithMouse();
        DoDamage();
    }
    void Moving()
    {
        float x = Input.GetAxisRaw("Horizontal") * 10f * Time.deltaTime;
        float z = Input.GetAxisRaw("Vertical") * 10f * Time.deltaTime;
        transform.Translate(x, 0, z);
    }

    private void RotateWithMouse()
    {
        Ray ray =  camera1.ScreenPointToRay(_mousepos);
        if (Physics.Raycast(ray,out RaycastHit hitInfo, maxDistance: 500f))
        {
            var target = hitInfo.point;
            target.y = transform.position.y;
            transform.LookAt(target);
        }
    }
    public void MousePosition(InputAction.CallbackContext context)
    {
        _mousepos = context.action.ReadValue<Vector2>();
    }
    
    [PunRPC]
    public void DoDamage()
    {
        var playerHealth = gameObject.GetComponent<PlayerUIManager>();
        //press key for damage
        if (Input.GetKeyDown(KeyCode.F))
        {
            playerHealth.healthP1 -= 10;
        }
    }
}