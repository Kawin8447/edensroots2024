using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChangeBackgroundColor : MonoBehaviour
{
    [SerializeField] private Image image;
    private Material _material;
    
    public float speed = 1f;
    
    void Start()
    {
        _material = image.material;
    }

    void FixedUpdate()
    {
        _material.SetFloat("_BGTime",Time.unscaledTime);
        
        float t = Mathf.PingPong(Time.time * speed, 1f);
        Color color = Color.HSVToRGB(t, 1, 1);
        _material.SetColor("_Color1", color);
    }
}
