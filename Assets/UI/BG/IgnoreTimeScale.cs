using UnityEngine;
using UnityEngine.UI;

public class IgnoreTimeScale : MonoBehaviour
{
    [SerializeField] private Image image;
    private Material _material;
    void Start()
    {
        _material = image.material;
    }

    void Update()
    {
        _material.SetFloat("_BGTime",Time.unscaledTime);
        //Debug.Log("Time:"+Time.unscaledTime);
    }
}

