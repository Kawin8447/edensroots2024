using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;
public class UIVariableStore : MonoBehaviour
{
    public enum UIPages
    {
        GameTitle,
        SelectMenu,
        SinglePlayer,
        MultiPlayer,
        None
    }
    
    //ตั้งชื่อจาก UIToolKit ใหม่
    #region UIToolKit Variables
    
    protected const string GameTitle = "V--Title";
    protected const string SelectMenu = "V--SelectMenu";
    protected const string SinglePlayer = "V--SinglePlayer";
    protected const string MultiPlayer = "V--MultiPlayer";
    protected const string ConfirmScreen = "V--Confirm";

    protected VisualElement _gameTitle;
    protected VisualElement _selectMenu;
    protected VisualElement _singlePlayer;
    protected VisualElement _multiPlayer;
    protected VisualElement _confirmScreen;

    protected const string ButtonStart = "Button--Start";
    protected const string ButtonSinglePlayer = "Button--SinglePlayer";
    protected const string ButtonMultiPlayer = "Button--MultiPlayer";
    protected const string ButtonExit = "Button--Exit";
    protected const string ButtonReturnSingle = "Button--Return--S";
    protected const string ButtonCampaignI = "Button--Campagin--I";
    protected const string ButtonCampaignIi = "Button--Campagin--II";
    protected const string ButtonCampaignIii = "Button--Campagin--III";
    protected const string ButtonReturnMulti = "Button--Return--M";
    protected const string ButtonHostRoom = "Button--Host";
    protected const string ButtonFindRoom = "Button--Find";
    protected const string ButtonYes = "Button--Yes";
    protected const string ButtonNo = "Button--No";

    protected Button _buttonStart;
    protected Button _buttonSinglePlayer;
    protected Button _buttonMultiPlayer;
    protected Button _buttonExit;
    protected Button _buttonReturnSingle;
    protected Button _buttonCampaignI;
    protected Button _buttonCampaignIi;
    protected Button _buttonCampaignIii;
    protected Button _buttonReturnMulti;
    protected Button _buttonHostRoom;
    protected Button _buttonFindRoom;
    protected Button _buttonYes;
    protected Button _buttonNo;

    protected const string TextWaring = "Warning";

    protected Label _textWarning;
    
    #endregion
    protected virtual void Start()
    {
        //เชื่อม container ใน UIToolKit กับ Script
        #region Button conection

        var root = GetComponent<UIDocument>().rootVisualElement;
        _gameTitle = root.Q<VisualElement>(GameTitle);
        _selectMenu = root.Q<VisualElement>(SelectMenu);
        _singlePlayer = root.Q<VisualElement>(SinglePlayer);
        _multiPlayer = root.Q<VisualElement>(MultiPlayer);
        _confirmScreen = root.Q<VisualElement>(ConfirmScreen);
        
        _buttonStart = root.Q<Button>(ButtonStart);
        _buttonSinglePlayer = root.Q<Button>(ButtonSinglePlayer);
        _buttonMultiPlayer = root.Q<Button>(ButtonMultiPlayer);
        _buttonExit = root.Q<Button>(ButtonExit);
        _buttonReturnSingle = root.Q<Button>(ButtonReturnSingle);
        _buttonCampaignI = root.Q<Button>(ButtonCampaignI);
        _buttonCampaignIi = root.Q<Button>(ButtonCampaignIi);
        _buttonCampaignIii = root.Q<Button>(ButtonCampaignIii);
        _buttonReturnMulti = root.Q<Button>(ButtonReturnMulti);
        _buttonHostRoom = root.Q<Button>(ButtonHostRoom);
        _buttonFindRoom = root.Q<Button>(ButtonFindRoom);
        _buttonYes = root.Q<Button>(ButtonYes);
        _buttonNo = root.Q<Button>(ButtonNo);

        _textWarning = root.Q<Label>(TextWaring);

        #endregion

    }
}

