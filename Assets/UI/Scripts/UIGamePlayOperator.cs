using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;

public class UIGamePlayOperator : UIGamePlayVariable
{
    [SerializeField] public CurrentElement CurrentUiElement = CurrentElement.None;

    private PlayerManager _playerStat;
    private GameObject playerManager;
    private bool DashOnCoolDown;
    private bool IsSet = false;
    private Color DiplayColor;
    public Color NoneColor;
    public Color EarthColor;
    public Color WaterColor;
    public Color WindColor;

    void Awake()
    {
        Invoke("FindPlayerManage", 0.1f);
    }

    void FindPlayerManage()
    {
        playerManager = GameObject.Find("PlayerManage(Clone)");
        _playerStat = playerManager.GetComponent<PlayerManager>();


        //false = ready to dash
        IsSet = true;

    }

    private void UpdateDashVisual()
    {
        if (DashOnCoolDown)
        {
            _dashVisual.AddToClassList("Dash--CoolDown");
        }
        else _dashVisual.RemoveFromClassList("Dash--CoolDown");
    }

    private void UpdateUltimateVVisual()
    {
        if (_playerStat.curUltEnergy >= _playerStat.MaxultEnergy)
        {
            _skillQ.RemoveFromClassList("Ulti--OnCoolDown");
            _qCoolDownScrim.style.display = DisplayStyle.None;
        }
        else
        {
            _skillQ.AddToClassList("Ulti--OnCoolDown");
            _qCoolDownScrim.style.display = DisplayStyle.Flex;
        }

    }
    void Update()
    {
        if (IsSet == true)
        {
            //init for linked data
            //Debug.Log("----->"+DashOnCoolDown);

            DashOnCoolDown = playerManager.GetComponentInChildren<PlayerControllerSys>().onDash;
            UpdateDashVisual();

            #region Player Health bar, Mana bar & current display element

            //Debug.Log(_playerStat.maxPlayerHp + " , "+_playerStat.playerHp);
            float currentHpPercent = (_playerStat.playerHp / _playerStat.maxPlayerHp) * 100;
            float overHealPercent = (_playerStat.playerHp - _playerStat.maxPlayerHp);
            //Debug.LogWarning(overHealPercent +" / " +currentHpPercent);
            if (currentHpPercent > _playerStat.maxPlayerHp)
            {
                _playerStat.playerHp -= 2f * Time.deltaTime;
            }
            _currentHp.style.width = Length.Percent(currentHpPercent);
            _overHeal.style.width = Length.Percent(overHealPercent);
            _currentHpText.text = Mathf.Round(_playerStat.playerHp) + " / " + _playerStat.maxPlayerHp;

            float currentManaPercent = (_playerStat.playerMana / _playerStat.maxPlayerMana) * 100;
            float manaOverFlow = (_playerStat.playerMana - _playerStat.maxPlayerMana);

            _currentMana.style.width = Length.Percent(currentManaPercent);
            _manaOverflow.style.width = Length.Percent(manaOverFlow);
            _currentManaText.text = _playerStat.playerMana + " / " + _playerStat.maxPlayerMana;
            
            switch (_playerStat._Element)
            {
                case (CurrentElement.None):
                    DiplayColor = NoneColor;
                    break;
                case (CurrentElement.Earth):
                    DiplayColor = EarthColor;
                    break;
                case (CurrentElement.Water):
                    DiplayColor = WaterColor;
                    break;
                case (CurrentElement.Wind):
                    DiplayColor = WindColor;
                    break;
            }
            _currentDisplayElement.style.backgroundColor = DiplayColor;

            #endregion

            #region Skill and Ultimate

            _eCoolDownScrim.style.display = DisplayStyle.None;
            _eSkillCharge.style.backgroundColor = DiplayColor;
            _eCoolDownTimeText.text = _playerStat.cooldownTime.ToString();

            _qCoolDownScrim.style.display = DisplayStyle.None;
            _qSkillCharge.style.backgroundColor = DiplayColor;
            _qCoolDownTimeText.text = _playerStat.curUltEnergy.ToString();
            
            float currentEnergyPercent = (_playerStat.curUltEnergy / _playerStat.MaxultEnergy) * 100;

            _qSkillCharge.style.height = Length.Percent(currentEnergyPercent);
            UpdateUltimateVVisual();

            #endregion


        }
    }
}
